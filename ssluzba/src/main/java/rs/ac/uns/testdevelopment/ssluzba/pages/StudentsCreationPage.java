package rs.ac.uns.testdevelopment.ssluzba.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class StudentsCreationPage {
	private WebDriver driver;

	public StudentsCreationPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement getModalDialog(){
		return driver.findElement(By.className("modal-dialog"));
	}
	
	public WebElement getModalTitle() {
		return driver.findElement(By.id("myStudentiLabel"));
	}
	public WebElement getIndex() {
		return driver.findElement(By.name("indeks"));
	}
	
	public void setIndex(String value){
		WebElement el = this.getIndex();
		el.clear();
		el.sendKeys(value);
	}
	
	public WebElement getIme() {
		return driver.findElement(By.name("ime"));
	}
	
	public void setIme(String value){
		WebElement el = this.getIme();
		el.clear();
		el.sendKeys(value);
	}
	
	public WebElement getPrezime() {
		return driver.findElement(By.name("prezime"));
	}
	
	public void setPrezime(String value){
		WebElement el = this.getPrezime();
		el.clear();
		el.sendKeys(value);
	}
	
	public WebElement getGrad() {
		return driver.findElement(By.name("grad"));
	}
	
	public void setGrad(String value){
		WebElement el = this.getGrad();
		el.clear();
		el.sendKeys(value);
	}
	
	public WebElement getCancelBtn(){
		return getModalDialog().findElement(By.className("btn-default"));
	}
	
	public WebElement getSaveBtn(){
		return getModalDialog().findElement(By.className("btn-primary"));
	}
	
	public void createStudent(String index, String ime, String prezime, String grad) {
		setIndex(index);
		setIme(ime);
		setPrezime(prezime);
		setGrad(grad);
		getSaveBtn().click();
	}

}
