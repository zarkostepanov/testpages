package primeri17;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class timeShift {

	public static void main(String[] args) {
		WebDriver browser = new ChromeDriver();
		browser.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		browser.manage().window().maximize();
		
		browser.navigate().to("https://tymeshiftdemo.tymeapp.com/schedule-main");
		
		String message = browser.findElement(By.className("subtitle")).getText();
		String expectedMessage = "Welcome back!";
		System.out.println(expectedMessage);
		
		WebElement username = browser.findElement(By.name("ZendLogin[username]"));
		WebElement password = browser.findElement(By.name("ZendLogin[password]"));
		WebElement btnLogin = browser.findElement(By.name("yt0"));
		
		System.out.println(username.isDisplayed());
		System.out.println(password.isDisplayed());
		System.out.println(btnLogin.isDisplayed());
		
		username.clear(); // delete current value
		username.sendKeys("pavel+autotester@tymeshift.com"); // send new value
		//Thread.sleep(2000);
		password.clear();
		password.sendKeys("TyMeShift321");
		//Thread.sleep(2000);
		// click Login button
		btnLogin.click();
	}

}
