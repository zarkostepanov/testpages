package primeri17;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class pinTest1 {

	public static void main(String[] args) {
		WebDriver browser = new ChromeDriver();
		browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		browser.manage().window().maximize();
		
		browser.navigate().to("https://partner.pinsoft.com/b2b/login.iface");
		
		String versionMessage = browser.findElement(By.className("version")).getText();
		String expectedVersionMessage = "v2.5.33";
		System.out.println(expectedVersionMessage);
		
		String message = browser.findElement(By.className("login_form_fild_cont_header")).getText();
		String expectedMessage = "Prijavite se na B2B sistem:";
		System.out.println(expectedMessage);
		
		WebElement username = browser.findElement(By.id("j_username"));
		WebElement password = browser.findElement(By.name("j_password"));
		WebElement btnLogin = browser.findElement(By.id("loginButtonId"));
		WebElement loginFormResetPass = browser.findElement(By.className("login_form_reset_pass"));
		WebElement btnPartnerLink = browser.findElement(By.className("partner_link"));
			
		System.out.println(username.isDisplayed());
		System.out.println(password.isDisplayed());
		System.out.println(btnLogin.isDisplayed());
		System.out.println(loginFormResetPass.isDisplayed());
		System.out.println(btnPartnerLink.isDisplayed());
		
		System.out.println("Korisničko ime: isEnabled " + browser.findElement(By.id("j_username")).isEnabled());
		System.out.println("Korisničko ime: isDisplayed " + browser.findElement(By.id("j_username")).isDisplayed());
		System.out.println("Lozinka isEnabled " + browser.findElement(By.id("j_username")).isEnabled());
		System.out.println("Lozinka: isDisplayed " + browser.findElement(By.id("j_username")).isDisplayed());
					
		username.clear(); // delete current value
		username.sendKeys("132zare@132.com"); // send new value
		//Thread.sleep(2000);
		password.clear();
		password.sendKeys("tester");
		//Thread.sleep(2000);
		// click Login button
		btnLogin.click();
		
		String errorMessage = browser.findElement(By.id("j_id37")).getText();
		String errorExpectedMessage = "Pogrešno korisničko ime ili lozinka! Obratite pažnju na velika i mala slova.";
		System.out.println(errorExpectedMessage);
		System.out.println("errorMessage isDisplayed " + browser.findElement(By.id("j_id37")).isDisplayed());
		
		browser.quit();
	}
	
}
