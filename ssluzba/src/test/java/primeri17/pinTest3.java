package primeri17;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class pinTest3 {

	public static void main(String[] args) {
		WebDriver browser = new ChromeDriver();
		browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		browser.manage().window().maximize();
		browser.navigate().to("https://partner.pinsoft.com/b2b/login.iface");
		java.util.List<WebElement> links = browser.findElements(By.tagName("a"));
		System.out.println("Number of Links in the Page is " + links.size());
		for (int i = 0; i < links.size(); ++i) {
			System.out.println("Name of Link# " + i + "-" + links.get(i).getText());
		}
		browser.close();
	}

}
