package primeri17;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class pinTest2 {

	public static void main(String[] args) {
		
		WebDriver browser = new ChromeDriver();
		browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		browser.manage().window().maximize();
		
		browser.navigate().to("https://partner.pinsoft.com/b2b/partner.iface");
		
		//Informacije o korisniku
		WebElement ime = browser.findElement(By.id("j_id15:firtNameInput"));
		WebElement prezime = browser.findElement(By.id("j_id15:j_id36"));
		WebElement korisnickoIme = browser.findElement(By.id("j_id15:j_id43"));
		WebElement ePosta = browser.findElement(By.id("j_id15:j_id50"));
		
		//Informacije o kompaniji
		WebElement naziv = browser.findElement(By.id("j_id15:j_id72"));
		WebElement adresa = browser.findElement(By.id("j_id15:j_id79"));
		WebElement link = browser.findElement(By.id("j_id15:j_id72"));
		WebElement telefon = browser.findElement(By.id("j_id15:j_id91"));
		
		//Spam zastita
		WebElement captImg = browser.findElement(By.id("j_id15:captchaImg"));
		WebElement btnRefresh = browser.findElement(By.id("j_id15:j_id111"));		
		WebElement btnSubmit = browser.findElement(By.id("j_id15:j_id124"));
	
		System.out.println(ime.isDisplayed());
		System.out.println(prezime.isDisplayed());
		System.out.println(korisnickoIme.isDisplayed());
		System.out.println(ePosta.isDisplayed());
		
		System.out.println(naziv.isDisplayed());
		System.out.println(adresa.isDisplayed());
		System.out.println(link.isDisplayed());
		System.out.println(telefon.isDisplayed());
		
		System.out.println(captImg.isDisplayed());
		System.out.println(captImg.isEnabled());		
		System.out.println(btnRefresh.isDisplayed());
		System.out.println(btnRefresh.isEnabled());
		System.out.println(btnSubmit.isDisplayed());
		System.out.println(btnSubmit.isEnabled());
		
		System.out.println("Ime : isEnabled " + browser.findElement(By.id("j_id15:firtNameInput")).isEnabled());
		System.out.println("Ime : isDisplayed " + browser.findElement(By.id("j_id15:firtNameInput")).isDisplayed());
		System.out.println("Prezime : isEnabled " + browser.findElement(By.id("j_id15:j_id36")).isEnabled());
		System.out.println("Prezime : isDisplayed " + browser.findElement(By.id("j_id15:j_id36")).isDisplayed());
		System.out.println("Korisnicko ime : isEnabled " + browser.findElement(By.id("j_id15:j_id43")).isEnabled());
		System.out.println("Korisnicko ime : isDisplayed " + browser.findElement(By.id("j_id15:j_id43")).isDisplayed());
		System.out.println("E-posta : isEnabled " + browser.findElement(By.id("j_id15:j_id50")).isEnabled());
		System.out.println("E-posta : isDisplayed " + browser.findElement(By.id("j_id15:j_id50")).isDisplayed());
		

		System.out.println("naziv : isEnabled " + browser.findElement(By.id("j_id15:j_id72")).isEnabled());
		System.out.println("naziv : isDisplayed " + browser.findElement(By.id("j_id15:j_id72")).isDisplayed());
		System.out.println("adresa : isEnabled " + browser.findElement(By.id("j_id15:j_id79")).isEnabled());
		System.out.println("adresa : isDisplayed " + browser.findElement(By.id("j_id15:j_id79")).isDisplayed());
		System.out.println("link : isEnabled " + browser.findElement(By.id("j_id15:j_id72")).isEnabled());
		System.out.println("link : isDisplayed " + browser.findElement(By.id("j_id15:j_id72")).isDisplayed());
		System.out.println("telefon : isEnabled " + browser.findElement(By.id("j_id15:j_id91")).isEnabled());
		System.out.println("telefon : isDisplayed " + browser.findElement(By.id("j_id15:j_id91")).isDisplayed());
				
		System.out.println("btnRefresh : isEnabled " + browser.findElement(By.id("j_id15:j_id111")).isEnabled());
		System.out.println("btnRefresh : isDisplayed " + browser.findElement(By.id("j_id15:j_id111")).isDisplayed());
		
		System.out.println("btnSubmit : isEnabled " + browser.findElement(By.id("j_id15:j_id124")).isEnabled());
		System.out.println("btnSubmit : isDisplayed " + browser.findElement(By.id("j_id15:j_id124")).isDisplayed());
		
		
		browser.quit();
	}

}
