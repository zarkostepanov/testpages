package primeri17;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Primer12 {

	public static void main(String[] args) {
	WebDriver browser = new FirefoxDriver();
	// Puts an Implicit wait, Will wait for 10 seconds
	// before throwing exception
	// instanciranje browsera
	// implicitno vreme cekanja za pronalazenje elemenata
	browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	// maximize window
	browser.manage().window().maximize();
	// navigate to adress
	browser.navigate().to("http://demowebshop.tricentis.com/");
	
	WebElement loginIn = browser.findElement(By.linkText("Log in"));
	System.out.println(loginIn.isDisplayed());
	loginIn.click(); 
	// go to login page
	
	WebElement email = browser.findElement(By.id("Email"));
	WebElement password = browser.findElement(By.id("Password"));
	WebElement checkBox = browser.findElement(By.id("RememberMe"));
	WebElement btnLogIn = browser.findElement(By.linkText("Log in"));
	

	// check elements visibility
	System.out.println(email.isDisplayed());
	System.out.println(password.isDisplayed());
	System.out.println(checkBox.isDisplayed());
	// check chexbox
	checkBox.click();

	System.out.println("The Check Box IsSelected " + browser.findElement(By.id("RememberMe")).isSelected());
	System.out.println("The Check Box IsEnabled " + browser.findElement(By.id("RememberMe")).isEnabled());
	System.out.println("The Check Box IsDisplayed " + browser.findElement(By.id("RememberMe")).isDisplayed());
	System.out.println(btnLogIn.isDisplayed());

	// set username value
	email.clear(); // delete current value
	email.sendKeys("zare_ns@yahoo.com"); // send new value

	// set password value
	password.clear();
	password.sendKeys("tester");
	
	// check chexbox
	checkBox.click();

	// click signin button
	btnLogIn.click();

	// get login message
	String message = browser.findElement(By.className("topic-html-content-header")).getText();
	String expectedMessage = "Welcome to our store";
	// check login message
	System.out.println(expectedMessage);
	// Shutdown the browser
	browser.quit();
	// Close the Browser.

		}

	}


